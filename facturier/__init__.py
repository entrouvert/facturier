__version__ = '1.4'

class Plugin(object):
    def get_before_urls(self):
        from . import urls
        return urls.urlpatterns

    def get_apps(self):
        return [__name__]

    def get_admin_modules(self):
        from . import dashboard
        return dashboard.get_admin_modules()
