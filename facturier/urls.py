from django.conf.urls import patterns, include, url
from .views import InvoiceView, InvoiceDownloadView, TransactionView, TransactionResponseListView

urlpatterns = patterns('',
    url(r'^facture/simple/(?P<slug>\w+)/(?P<id>\w+)/(?P<hash>\w+)$',
        InvoiceView.as_view(),
        name='invoice'),
    url(r'^facture/details/(?P<slug>\w+)/(?P<id>\w+)/(?P<hash>\w+)$',
        InvoiceDownloadView.as_view(),
        name='invoice-download'),
    url(r'^facture/transaction/(?P<slug>\w+)/(?P<id>\w+)/(?P<hash>\w+)$',
        TransactionView.as_view(),
        name='transaction'),
    url(r'^facture/responses/(?P<regie>\w+).(?P<days>\d+).csv$',
        TransactionResponseListView.as_view(),
        name='responses'),
)
