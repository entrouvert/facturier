# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Regie'
        db.create_table(u'facturier_regie', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('label', self.gf('django.db.models.fields.CharField')(max_length=64)),
            ('slug', self.gf('django.db.models.fields.SlugField')(unique=True, max_length=50)),
            ('description', self.gf('django.db.models.fields.TextField')()),
            ('service', self.gf('django.db.models.fields.CharField')(max_length=64)),
            ('get_url', self.gf('django.db.models.fields.related.ForeignKey')(related_name='regie_get', to=orm['data_source_plugin.DataSource'])),
            ('update_url', self.gf('django.db.models.fields.related.ForeignKey')(related_name='regie_update', to=orm['data_source_plugin.DataSource'])),
        ))
        db.send_create_signal(u'facturier', ['Regie'])

        # Adding model 'ServiceOption'
        db.create_table(u'facturier_serviceoption', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('regie', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['facturier.Regie'])),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=32)),
            ('value', self.gf('django.db.models.fields.CharField')(max_length=256)),
        ))
        db.send_create_signal(u'facturier', ['ServiceOption'])

        # Adding model 'RequestOption'
        db.create_table(u'facturier_requestoption', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('regie', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['facturier.Regie'])),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=32)),
            ('value', self.gf('django.db.models.fields.CharField')(max_length=256)),
        ))
        db.send_create_signal(u'facturier', ['RequestOption'])

        # Adding model 'TransactionEvent'
        db.create_table(u'facturier_transactionevent', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('transaction_id', self.gf('django.db.models.fields.CharField')(max_length=128, db_index=True)),
            ('invoice_id', self.gf('django.db.models.fields.CharField')(max_length=128, db_index=True)),
            ('date', self.gf('django.db.models.fields.DateTimeField')(auto_now_add=True, blank=True)),
            ('event', self.gf('django.db.models.fields.CharField')(max_length=16)),
            ('details', self.gf('django.db.models.fields.TextField')(null=True, blank=True)),
        ))
        db.send_create_signal(u'facturier', ['TransactionEvent'])


    def backwards(self, orm):
        # Deleting model 'Regie'
        db.delete_table(u'facturier_regie')

        # Deleting model 'ServiceOption'
        db.delete_table(u'facturier_serviceoption')

        # Deleting model 'RequestOption'
        db.delete_table(u'facturier_requestoption')

        # Deleting model 'TransactionEvent'
        db.delete_table(u'facturier_transactionevent')


    models = {
        u'data_source_plugin.datasource': {
            'Meta': {'ordering': "('name',)", 'object_name': 'DataSource'},
            'allow_redirects': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'hash_algo': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '16', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'mime_type': ('django.db.models.fields.CharField', [], {'max_length': '256'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '32'}),
            'signature_key': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '128', 'blank': 'True'}),
            'timeout': ('django.db.models.fields.IntegerField', [], {'default': '10'}),
            'url': ('django.db.models.fields.CharField', [], {'max_length': '1024'}),
            'verify_certificate': ('django.db.models.fields.BooleanField', [], {'default': 'True'})
        },
        u'facturier.regie': {
            'Meta': {'object_name': 'Regie'},
            'description': ('django.db.models.fields.TextField', [], {}),
            'get_url': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'regie_get'", 'to': u"orm['data_source_plugin.DataSource']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'label': ('django.db.models.fields.CharField', [], {'max_length': '64'}),
            'service': ('django.db.models.fields.CharField', [], {'max_length': '64'}),
            'slug': ('django.db.models.fields.SlugField', [], {'unique': 'True', 'max_length': '50'}),
            'update_url': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'regie_update'", 'to': u"orm['data_source_plugin.DataSource']"})
        },
        u'facturier.requestoption': {
            'Meta': {'object_name': 'RequestOption'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '32'}),
            'regie': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['facturier.Regie']"}),
            'value': ('django.db.models.fields.CharField', [], {'max_length': '256'})
        },
        u'facturier.serviceoption': {
            'Meta': {'object_name': 'ServiceOption'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '32'}),
            'regie': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['facturier.Regie']"}),
            'value': ('django.db.models.fields.CharField', [], {'max_length': '256'})
        },
        u'facturier.transactionevent': {
            'Meta': {'object_name': 'TransactionEvent'},
            'date': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'details': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'event': ('django.db.models.fields.CharField', [], {'max_length': '16'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'invoice_id': ('django.db.models.fields.CharField', [], {'max_length': '128', 'db_index': 'True'}),
            'transaction_id': ('django.db.models.fields.CharField', [], {'max_length': '128', 'db_index': 'True'})
        }
    }

    complete_apps = ['facturier']