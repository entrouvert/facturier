# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Deleting field 'Regie.get_url'
        db.delete_column(u'facturier_regie', 'get_url_id')

        # Deleting field 'Regie.get_list_url'
        db.delete_column(u'facturier_regie', 'get_list_url_id')

        # Deleting field 'Regie.update_url'
        db.delete_column(u'facturier_regie', 'update_url_id')

        # Adding field 'Regie.invoice_get_url'
        db.add_column(u'facturier_regie', 'invoice_get_url',
                      self.gf('django.db.models.fields.CharField')(default='', max_length=256),
                      keep_default=False)

        # Adding field 'Regie.invoice_update_url'
        db.add_column(u'facturier_regie', 'invoice_update_url',
                      self.gf('django.db.models.fields.CharField')(default='', max_length=256),
                      keep_default=False)

        # Adding field 'Regie.invoice_list_url'
        db.add_column(u'facturier_regie', 'invoice_list_url',
                      self.gf('django.db.models.fields.CharField')(max_length=256, null=True, blank=True),
                      keep_default=False)


    def backwards(self, orm):
        # Adding field 'Regie.get_url'
        db.add_column(u'facturier_regie', 'get_url',
                      self.gf('django.db.models.fields.related.ForeignKey')(default='', related_name='regie_get', to=orm['data_source_plugin.DataSource']),
                      keep_default=False)

        # Adding field 'Regie.get_list_url'
        db.add_column(u'facturier_regie', 'get_list_url',
                      self.gf('django.db.models.fields.related.ForeignKey')(related_name='regie_get_list', null=True, to=orm['data_source_plugin.DataSource'], blank=True),
                      keep_default=False)

        # Adding field 'Regie.update_url'
        db.add_column(u'facturier_regie', 'update_url',
                      self.gf('django.db.models.fields.related.ForeignKey')(default='', related_name='regie_update', to=orm['data_source_plugin.DataSource']),
                      keep_default=False)

        # Deleting field 'Regie.invoice_get_url'
        db.delete_column(u'facturier_regie', 'invoice_get_url')

        # Deleting field 'Regie.invoice_update_url'
        db.delete_column(u'facturier_regie', 'invoice_update_url')

        # Deleting field 'Regie.invoice_list_url'
        db.delete_column(u'facturier_regie', 'invoice_list_url')


    models = {
        u'facturier.regie': {
            'Meta': {'object_name': 'Regie'},
            'description': ('django.db.models.fields.TextField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'invoice_get_url': ('django.db.models.fields.CharField', [], {'max_length': '256'}),
            'invoice_list_url': ('django.db.models.fields.CharField', [], {'max_length': '256', 'null': 'True', 'blank': 'True'}),
            'invoice_update_url': ('django.db.models.fields.CharField', [], {'max_length': '256'}),
            'label': ('django.db.models.fields.CharField', [], {'max_length': '64'}),
            'service': ('django.db.models.fields.CharField', [], {'max_length': '64'}),
            'slug': ('django.db.models.fields.SlugField', [], {'unique': 'True', 'max_length': '50'})
        },
        u'facturier.requestoption': {
            'Meta': {'object_name': 'RequestOption'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '32'}),
            'regie': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['facturier.Regie']"}),
            'value': ('django.db.models.fields.CharField', [], {'max_length': '256'})
        },
        u'facturier.serviceoption': {
            'Meta': {'object_name': 'ServiceOption'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '32'}),
            'regie': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['facturier.Regie']"}),
            'value': ('django.db.models.fields.CharField', [], {'max_length': '256'})
        },
        u'facturier.transactionevent': {
            'Meta': {'ordering': "['date']", 'object_name': 'TransactionEvent'},
            'address': ('django.db.models.fields.GenericIPAddressField', [], {'max_length': '39', 'null': 'True', 'blank': 'True'}),
            'date': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'details': ('jsonfield.fields.JSONField', [], {'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'invoice_id': ('django.db.models.fields.CharField', [], {'max_length': '128', 'db_index': 'True'}),
            'nameid': ('django.db.models.fields.CharField', [], {'max_length': '256', 'null': 'True', 'blank': 'True'}),
            'regie': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['facturier.Regie']"}),
            'response': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'status': ('django.db.models.fields.CharField', [], {'default': "'CREATED'", 'max_length': '16'}),
            'transaction_id': ('django.db.models.fields.CharField', [], {'max_length': '128', 'db_index': 'True'})
        }
    }

    complete_apps = ['facturier']