# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Deleting field 'TransactionEvent.event'
        db.delete_column(u'facturier_transactionevent', 'event')

        # Adding field 'TransactionEvent.status'
        db.add_column(u'facturier_transactionevent', 'status',
                      self.gf('django.db.models.fields.CharField')(default='', max_length=16),
                      keep_default=False)


    def backwards(self, orm):
        # Adding field 'TransactionEvent.event'
        db.add_column(u'facturier_transactionevent', 'event',
                      self.gf('django.db.models.fields.CharField')(default='', max_length=16),
                      keep_default=False)

        # Deleting field 'TransactionEvent.status'
        db.delete_column(u'facturier_transactionevent', 'status')


    models = {
        u'data_source_plugin.datasource': {
            'Meta': {'ordering': "('name',)", 'object_name': 'DataSource'},
            'allow_redirects': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'hash_algo': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '16', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'mime_type': ('django.db.models.fields.CharField', [], {'max_length': '256'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '32'}),
            'signature_key': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '128', 'blank': 'True'}),
            'timeout': ('django.db.models.fields.IntegerField', [], {'default': '10'}),
            'url': ('django.db.models.fields.CharField', [], {'max_length': '1024'}),
            'verify_certificate': ('django.db.models.fields.BooleanField', [], {'default': 'True'})
        },
        u'facturier.regie': {
            'Meta': {'object_name': 'Regie'},
            'description': ('django.db.models.fields.TextField', [], {}),
            'get_url': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'regie_get'", 'to': u"orm['data_source_plugin.DataSource']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'label': ('django.db.models.fields.CharField', [], {'max_length': '64'}),
            'service': ('django.db.models.fields.CharField', [], {'max_length': '64'}),
            'slug': ('django.db.models.fields.SlugField', [], {'unique': 'True', 'max_length': '50'}),
            'update_url': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'regie_update'", 'to': u"orm['data_source_plugin.DataSource']"})
        },
        u'facturier.requestoption': {
            'Meta': {'object_name': 'RequestOption'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '32'}),
            'regie': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['facturier.Regie']"}),
            'value': ('django.db.models.fields.CharField', [], {'max_length': '256'})
        },
        u'facturier.serviceoption': {
            'Meta': {'object_name': 'ServiceOption'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '32'}),
            'regie': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['facturier.Regie']"}),
            'value': ('django.db.models.fields.CharField', [], {'max_length': '256'})
        },
        u'facturier.transactionevent': {
            'Meta': {'object_name': 'TransactionEvent'},
            'date': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'details': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'invoice_id': ('django.db.models.fields.CharField', [], {'max_length': '128', 'db_index': 'True'}),
            'status': ('django.db.models.fields.CharField', [], {'max_length': '16'}),
            'transaction_id': ('django.db.models.fields.CharField', [], {'max_length': '128', 'db_index': 'True'})
        }
    }

    complete_apps = ['facturier']