from django import template
from decimal import Decimal

register = template.Library()

@register.filter
def decimal(value):
    return Decimal(value)

