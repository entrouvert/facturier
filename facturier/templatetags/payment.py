from django import template

register = template.Library()

def payment_url(invoice_id, invoice_hash):
    return reverse('transaction', args=('tipi', invoice_id, invoice_hash))

