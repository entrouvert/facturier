from django.core.management.base import BaseCommand
from django.utils import timezone

from facturier.models import TransactionEvent


class Command(BaseCommand):

    def handle(self, *args, **options):
        delta = timezone.now() - timezone.timedelta(minutes=10)
        for event in TransactionEvent.objects.filter(status='CREATED', date__lte=delta):
            if not TransactionEvent.objects.filter(invoice_id=event.invoice_id, date__gt=event.date,
                                               status__in=['CANCELED', 'ERROR', 'PAID', 'DENIED']).exists():
                TransactionEvent.objects.create(transaction_id=event.transaction_id,
                                                status='CANCELED',
                                                invoice_id=event.invoice_id, regie=event.regie,
                                                details='{"message": "%s canceled because expired"}' % event.transaction_id)
