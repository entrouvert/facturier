import sys

from django.conf import settings
from django.core.exceptions import ImproperlyConfigured

class AppSettings(object):
    '''Thanks you, django-allauth'''
    __SENTINEL = object()

    def __init__(self, prefix):
        self.prefix = prefix

    def _setting(self, name, dflt=__SENTINEL):
        from django.conf import settings
        v  = getattr(settings, self.prefix + name, dflt)
        if v is self.__SENTINEL:
            raise ImproperlyConfigured('Missing setting %r' % (self.prefix + name))
        return v

    @property
    def download_filename(self):
        return self._setting('DOWNLOAD_FILENAME',
                             '/var/lib/invoices/invoice_{invoice[id]}.pdf')

    @property
    def download_filename_mimetype(self):
        return self._setting('DOWNLOAD_FILENAME_MIMETYPE', 'application/pdf')

    @property
    def responses_csv_key(self):
        return self._setting('RESPONSES_CSV_KEY')


app_settings = AppSettings('FACTURIER_')
app_settings.__name__ = __name__
sys.modules[__name__] = app_settings
