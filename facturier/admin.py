from .models import Regie, ServiceOption, RequestOption
from .models import TransactionEvent

from django.contrib import admin

class RequestOptionInline(admin.TabularInline):
    model = RequestOption

class ServiceOptionInline(admin.TabularInline):
    model = ServiceOption

class RegieAdmin(admin.ModelAdmin):
    prepopulated_fields = {"slug": ("label",)}
    list_display = ('label', 'service')
    list_filter = ('service',)
    inlines = (ServiceOptionInline, RequestOptionInline)
    extras = 3

class TransactionEventAdmin(admin.ModelAdmin):
    date_hierarchy = 'date'
    list_display = ('date', 'transaction_id', 'regie', 'invoice_id', 'status',
            'response', 'details')
    list_filter = ('regie', 'response')
    readonly_fields = ('date', 'transaction_id', 'regie', 'invoice_id',
            'status', 'response', 'details', 'address', 'nameid')
    search_fields = ('invoice_id', 'details', 'address', 'nameid')

admin.site.register(Regie, RegieAdmin)
admin.site.register(TransactionEvent, TransactionEventAdmin)
