from datetime import datetime
from decimal import Decimal

from django.db import models
from django.core.urlresolvers import reverse
from django.utils.translation import ugettext_lazy as _, pgettext_lazy
from django.template import RequestContext
from django.template import Template

from jsonfield import JSONField

from cmsplugin_blurp.renderers.data_source import Data

import requests
import eopayment

SERVICES = ((eopayment.SIPS, 'SIPS'),
            (eopayment.SYSTEMPAY, 'SYSTEMPAY'),
            (eopayment.SPPLUS, 'SPPLUS'),
            (eopayment.TIPI, 'TIPI'),
            (eopayment.DUMMY, 'DUMMY'),
            )

TRANSACTION_STATUS_PROD = (
    ('CREATED', 'CREATED'),
    ('PAID', 'PAID'),
    ('DENIED', 'DENIED'),
    ('CANCELED', 'CANCELED'),
    ('ERROR', 'ERROR'),
)
TRANSACTION_STATUS_TEST = tuple((('TEST_%s' % k, 'TEST_%s' %v)
        for k,v in TRANSACTION_STATUS_PROD))
TRANSACTION_STATUS = TRANSACTION_STATUS_PROD + TRANSACTION_STATUS_TEST


class Regie(models.Model):
    label = models.CharField(max_length=64)
    slug = models.SlugField(unique=True)
    description = models.TextField()
    service = models.CharField(max_length=64, choices=SERVICES)
    invoice_get_url = models.CharField(max_length=256, help_text=_('Get an invoice from this Regie'))
    invoice_update_url = models.CharField(max_length=256, help_text=_('Update an invoice of this Regie'))
    invoice_list_url = models.CharField(max_length=256, blank=True, null=True,
            help_text=_("Get a list of invoices "\
                        "(RequestContext dependent, typically user's invoices)"))

    def natural_key(self):
        return (self.slug,)

    def __unicode__(self):
        return self.label

    def get_invoice_url(self, context):
        return Template(self.invoice_get_url).render(context)

    def get_invoice_update_url(self, context):
        return Template(self.invoice_update_url).render(context)

    def get_invoice_list_url(self, context):
        return Template(self.invoice_list_url).render(context)

    def get_invoice_list(self, request, **kwargs):
        if not self.invoice_list_url:
            return []
        context = RequestContext(request, kwargs)
        data_source = Data(self.slug, {'limit': None, 'refresh': None},
                           {'url': self.get_invoice_list_url(context),
                            'content_type': 'application/json',
                            'parser_type': 'json',
                            'auth_mech': 'oauth2',
                            'redirects': True,
                            'slug': 'invoice_list'}, context)
        content = data_source.update_content()
        return content.get('data', {}).get('invoices', [])

    def get_invoice(self, invoice_id, invoice_hash=None, request=None):
        '''get an invoice (dict), via webservice
        invoice_hash: a key to access the invoice (for example a password)
        '''
        context = {'regie': self,
                'invoice_id': invoice_id,
                'invoice_hash': invoice_hash}
        if request:
            context = RequestContext(request, context)
        data_source = Data(self.slug, {'limit': None, 'refresh': None},
                           {'url': self.get_invoice_url(context),
                            'content_type': 'application/json',
                            'parser_type': 'json',
                            'slug': 'invoice'}, context)
        content = data_source.update_content()
        invoice = content.get('data', {}).get('invoice', {})
        if invoice:
            invoice['url'] = reverse('transaction',
                args=(self.slug, invoice_id, invoice_hash))
            # is this invoice in the list of invoices ? (typically, list
            # of user's invoices). If yes, add a "download_url" value
            if self.invoice_list_url and request and request.user.is_authenticated():
                invoice['in_list'] = False
                ctx_invoices = self.get_invoice_list(request)
                for i in ctx_invoices:
                    if i['id'] == invoice['id']:
                        invoice['download_url'] = reverse('invoice-download',
                            args=(self.slug, invoice_id, invoice_hash))
                        break
            # convert values to proper datatypes
            for amount_key in ('paid_amount', 'total_amount', 'amount'):
                if invoice.get(amount_key):
                    invoice[amount_key] = Decimal(invoice[amount_key])
            for date_key in ('creation_date', 'expiration_date', 'paid_date',
                    'tipi_paid_date'):
                if invoice.get(date_key):
                    invoice[date_key] = datetime.strptime(invoice[date_key], '%Y-%m-%d')
        return invoice


class Option(models.Model):
    regie = models.ForeignKey(Regie)
    name = models.CharField(max_length=32)
    value = models.CharField(max_length=256)

    class Meta:
        abstract = True

    def __unicode__(self):
        return _('%s: %s') % (self.name, self.value)

class ServiceOption(Option):
    pass

class RequestOption(Option):
    pass

class TransactionEvent(models.Model):
    regie = models.ForeignKey(Regie)
    transaction_id = models.CharField(max_length=128, db_index=True)
    invoice_id= models.CharField(max_length=128, db_index=True)
    date = models.DateTimeField(auto_now_add=True)
    address = models.GenericIPAddressField(null=True, blank=True)
    nameid = models.CharField(max_length=256, null=True, blank=True)
    status = models.CharField(max_length=16, choices=TRANSACTION_STATUS,
                              default='CREATED')
    response = models.BooleanField(default=False)
    details = JSONField(blank=True, null=True)

    class Meta:
        get_latest_by = 'date'
        ordering = ['date']
        verbose_name = _('Transaction event')
        verbose_name_plural = _('Transaction events')

